# xapi-videojs
This  is a reference implementation of the xAPI Video Profile integrated with the [VideoJS player](https://videojs.com/) (video player library that uses web standards).

It provide a demo of using HTML5 video embeds (.mp4, .ogv, .webm) for xAPI video tracking. 

Video Profile Documentation: https://liveaspankaj.gitbooks.io/xapi-video-profile/content/.   

## Configuration
xAPI Video JS binds itself to a global ADL object, and is able to access and communicate to the xAPI Wrapper. The xAPI Wrapper and xAPI Video JS can be included in any HTML page, and configured to track interactions with a video on that page.

1. Include the required scripts in the &lt;head&gt; of the HTML file.
    ``` html
    <link href="video-js-6.1.0/video-js.css" rel="stylesheet">
    <script type="text/javascript" src="url-polyfill.min.js"></script>
    <script src="video-js-6.1.0/ie8/videojs-ie8.min.js"></script>
    <script src="video-js-6.1.0/video.js"></script>
    <script type="text/javascript" src="xapiwrapper.min.js"></script>
    <script type="text/javascript" src="xapi-videojs.js"></script>
    ```
2. Set the 'endpoint', 'auth', and 'actor' properties of the xAPI Wrapper using the changeConfig() function.
    ``` javascript
    var newconf = {
        "endpoint" : "<your lrs endpoint url>",
        "auth" : "Basic " + toBase64('your:creds'),
        "actor" : '{"mbox": "mailto:user@example.com","name": "example user","objectType": "Agent"}'   
    };
    ADL.XAPIWrapper.changeConfig(newconf);
    ```
3. Create an activityTitle variable to the title of the video.
    ``` javascript
    var activityTitle = document.getElementById('vidtitle').getAttribute('content');
    ```
4. Create an activityDesc variable to the description of the video.
    ``` javascript
    var activityDesc = document.getElementById('viddesc').getAttribute('content'); 
    ```
5. Call the XAPIVideoJS constructor and pass the id of the video.
    ``` javascript
    ADL.XAPIVideoJS("vid1");
    ```

See index.html for an example of how to configure the xAPI Video JS library.

### Launch Configuration
The example index.html also demonstrates support for [ADL Launch](https://github.com/adlnet/xapi-launch) and a subset of properties from [TinCan Launch](https://github.com/RusticiSoftware/launch/blob/master/lms_lrs.md) - using endpoint, auth, actor, and registration. No changes are necessary to use the launch feature, and will work as is if launched using one of those two techniques.

> **NOTE**: Even if using launch the video url, activity title and activity description need to be defined in the HTML file.

#### To use ADL Launch
Call ADL.launch() and pass in a callback function to invoke after the Launch process.

``` javascript
ADL.launch(function (err, launchdata, xAPIWrapper) {
    if (!err) {
        // this was adl launched, use newly configured wrapper
        ADL.XAPIWrapper = xAPIWrapper;
    } else {
        // not adl launched
        var newconf = {
            "endpoint" : "<your lrs endpoint url>",
            "auth" : "Basic " + toBase64('your:creds'),
            "actor" : '{"mbox": "mailto:user@example.com","name": "example user","objectType": "Agent"}'   
        };
        ADL.XAPIWrapper.changeConfig(newconf);
    }
    
    ADL.XAPIVideoJS("vid1");

}, true);
```  

#### To Use TinCan Launch
TinCan Launch applies launch configuration values to the URL as URL parameters. Included in the index.html example is a function `parseLaunchParams` that will take the 'endpoint', 'auth' and 'actor' params from the command line and add them to a configuration object which can be passed to the `ADL.XAPIWrapper.changeConfig` function.

```javascript
var newconf = {
    "endpoint" : "https://tlatest.lrs.io/xapi/",
    "auth" : "Basic " + toBase64('fleas:flew'),
    "actor" : '{"mbox": "mailto:tom@example.com","name": "tom","objectType": "Agent"}'   
};
ADL.XAPIWrapper.changeConfig(parseLaunchParams(new URL(window.location.href), newconf));
```

## Installation
xAPI Video JS is a bundle of HTML and JS files. No true installation steps are required - include in SCORM packages, or host on servers. Just ensure to include all required files:
* /captions/ - if closed captions are supported, this folder contains the closed captions file
* /video-js-6.1.0/ - the video player library
* /index.html - the HTML page with the embedded video
* /url-polyfill.min.js - lib used when the browser doesn't support the URL api
* /xapi-videojs.js - lib that monitors video interactions and generates xapi-video profile statements
* /xapiwrapper.min.js - ADL xAPI Wrapper lib used to communicate with an LRS, sending and retrieving statements

## Use
* Navigate to the URL of where the video is hosted, or launch the SCO which contains the video
* Click the play button
* Sliding the control under the video will scrub the video playback to the new location
* Turn closed captions on/off by clicking the 'cc' icon in the lower right of the video player
